# Openbox Blocks-Dark

A dark version of the "Blocks" theme found on [github.com/addy-dclxvi/openbox-theme-collections](https://github.com/addy-dclxvi/openbox-theme-collections).

![](./looks.png)
